import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  nombre:string = 'Ramiros Santos';
  nombre2:string = 'MaxiMiliAno PaReDes';
  array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  PI: number = Math.PI;
  porcentaje:number = 0.234;
  salario: number = 1234.5;

  fecha: Date = new Date();

  heroe:any = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    direccion: {
      calle: 'Las vertientes #478',
      zone: 'Los Horizontes',
    }
  }

  valorPromesa = new Promise<string>((resolve) => {
    setTimeout(() => {
      resolve ('Llego la data')
    }, 3500); //entra aqui y se quedara pausado ese tiempo que es 3,5 seg
  });


  constructor() { }

  ngOnInit(): void {
  }

}
